package ru.tsc.kyurinova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractOwnerDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskDTO findByUserIdAndProjectIdAndId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void deleteAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
