package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractOwnerRepository<Task> {

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findByUserIdAndProjectIdAndId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void deleteAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
