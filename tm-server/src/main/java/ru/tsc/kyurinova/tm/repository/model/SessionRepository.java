package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.Session;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractRepository<Session> {

    Optional<Session> findById(@NotNull String id);

    @Nullable
    List<Session> findAll();

    void deleteById(@NotNull String id);

    long count();

}
