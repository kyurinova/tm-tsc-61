package ru.tsc.kyurinova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.repository.dto.ProjectDTORepository;
import ru.tsc.kyurinova.tm.repository.dto.TaskDTORepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    @Autowired
    public ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    public TaskDTORepository taskRepository;

    @Override
    @Transactional
    public void bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        ProjectDTO project = projectRepository.findByUserIdAndId(userId, projectId);
        TaskDTO task = taskRepository.findByUserIdAndId(userId, taskId);
        if (project == null) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        ProjectDTO project = projectRepository.findByUserIdAndId(userId, projectId);
        TaskDTO task = taskRepository.findByUserIdAndId(userId, taskId);
        if (project == null) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        if (task.getProjectId().equals(projectId))
            task.setProjectId("");
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        ProjectDTO project = projectRepository.findByUserIdAndId(userId, projectId);
        projectRepository.delete(project);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllTaskByProjectId(@Nullable final String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

}
