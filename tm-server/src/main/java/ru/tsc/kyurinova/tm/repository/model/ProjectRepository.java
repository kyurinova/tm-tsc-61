package ru.tsc.kyurinova.tm.repository.model;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.Project;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractOwnerRepository<Project> {

}
