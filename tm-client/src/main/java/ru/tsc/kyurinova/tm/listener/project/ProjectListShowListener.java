package ru.tsc.kyurinova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.enumerated.Sort;
import ru.tsc.kyurinova.tm.endpoint.ProjectDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class ProjectListShowListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    @EventListener(condition = "@projectListShowListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable final List<ProjectDTO> projects;
        System.out.println("[LIST PROJECTS]");
        if (sort == null || sort.isEmpty())
            projects = projectEndpoint.findAllProject(sessionService.getSession());
        else {
            projects = projectEndpoint.findAllProjectsSorted(sessionService.getSession(), sort);
        }
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " + project.getId() + ". " + project.getName() + ". " + project.getDescription() + ". " + project.getStatus() + ". " + project.getStartDate());
        }
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
